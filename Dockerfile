FROM node:5.0-slim
MAINTAINER hhao

RUN npm install gulp -g
COPY package.json /
COPY gulpfile.js /
COPY index.html /
RUN npm install
EXPOSE 80
CMD ["gulp"]